# Docker PHP-FPM
A Dockerfile used to create a custom PHP-FPM Docker image for local use.

## Modules
* Headers
* Rewrite

## Extensions
* Mysqli
* ImageMagick

## Tools
* WP-CLI

## Rights
* Change the UID of www-data to 1000
* Add www-data to group staff
